module modernc.org/sqlite

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.8
	golang.org/x/sys v0.0.0-20210902050250-f475640dd07b
	modernc.org/ccgo/v3 v3.12.17
	modernc.org/libc v1.11.23
	modernc.org/mathutil v1.4.1
	modernc.org/tcl v1.7.3
	modernc.org/z v1.2.5
)
